
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:login_register/controlador/controlador.dart';
import 'package:login_register/models/alimentacion.dart';
import 'package:login_register/models/producto.dart';
import 'package:login_register/models/usuario.dart';
import 'package:login_register/screens/contador_calorias_screen.dart';


void main() {
      //setUpAll carga variables de entorno en el archivo .env
  setUpAll(() async {
    await dotenv.load(fileName: ".env");
  });
  test('userRegister test', () async {
    final username = 'TestUser';
    final email = 'example@example.com';
    final password = 'password';
    

    final responseData = {'Status': 'Usuario guardado'};

    final result = await Controlador.userRegister(username, email, password);

    expect(result, isA<dynamic>());
    expect(result, responseData);
  });
  test('userLogin test', () async {
    final email = 'example@example.com';
    final password = 'password';


    final result = await Controlador.userLogin(email, password);

    expect(result, isA<dynamic>());
  });
test('addProduct test', () async {
  final name = 'manzana';
  final calorias = 20.00;
  final proteinas = 19.99;
  final carbohidratos = 19.99;
  final grasas = 19.99;

  try {
    final result = await Controlador.addItemproducts(name, calorias, proteinas, carbohidratos, grasas, 1);
    
    expect(result, isA<dynamic>());
  
  } catch (e) {
    fail('La prueba falló debido a una excepción: $e');
  }
});

  test('editProduct test', () async {
    final producto_id = 1;
    final name = 'Nuevo Producto';
    final calorias = 20.00;
    final proteinas = 19.99;
    final carbohidratos = 19.99;
    final grasas = 19.99;
    final usuario_id = 1;

    final result = await Controlador.updateItemproducto(producto_id, name, calorias, proteinas, carbohidratos, grasas, usuario_id);
    expect(result, isA<Map<String, dynamic>>());
    expect(result['Status'], 'Producto actualizado');
  });
   test('Calculo de Calorias', () {
    final contadorDeCalorias = ContadorDeCaloriasState();

    contadorDeCalorias.alimentos = [
      Alimentacion(
        id: 11,
        producto: Producto(
          id: '10',
          nombre: 'manzana',
          calorias: 1.0,
          proteinas: 1.0,
          carbohidratos: 1.0,
          grasas: 1.0,
          idUsuario: '2',
        ),
        cantidad: 1,
        fecha: '2024-02-01',
        usuario: Usuario( 
          id: '2',
          nombre: 'Usuario 2',
          email: 'usuario2@example.com',
          password: 'contraseña',
        ),
      ),
      Alimentacion(
        id: 12,
        producto: Producto(
          id: '10',
          nombre: 'manzana',
          calorias: 1.0,
          proteinas: 1.0,
          carbohidratos: 1.0,
          grasas: 1.0,
          idUsuario: '1',
        ),
        cantidad: 1,
        fecha: '2024-02-01',
        usuario: Usuario(
          id: '1',
          nombre: 'Usuario 1',
          email: 'usuario1@example.com',
          password: 'contraseña',
        ),
      ),
    ];
    //se llama a la función calcularCaloriasTotales de la clase ContadorDeCaloriasState lo cual suma las calorias de los alimentos y devuelve un total :v
    final result = contadorDeCalorias.calcularCaloriasTotales();
    
    //verifica que el resultado sea igual a 2.0 xd
    final expectedCalories = 2.0;  

    expect(result, equals(expectedCalories));
  });
}