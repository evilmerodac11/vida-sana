import 'dart:convert';

import 'package:login_register/constant/utils.dart';
import 'package:login_register/models/usuario.dart';
import 'package:login_register/models/producto.dart';
import 'package:login_register/models/alimentacion.dart';
import 'package:http/http.dart' as http;

class Controlador {
  static Usuario? user;

  static List<Producto> productosMock = [];

  static List<Alimentacion> alimentacionesMock = [];
  static Future<List<dynamic>> getAllProductos() async {
    final response = await http.get(
      Uri.parse('${Utils.baseUrl}/api/products/all'),
      headers: {"Accept": "Application/json"},
    );

    var decodedData = jsonDecode(response.body);
    print(response.body);
    print(decodedData);
    productosMock = [];
    // Verifica si decodedData es una lista
    if (decodedData is List) {
      // Recorre cada elemento de la lista
      decodedData.forEach((producto) {
        // Realiza alguna operación con cada producto
        print(producto);
        productosMock.add(Producto.fromMap(producto));
      });
      return decodedData;
    } else {
      throw Exception('Error: los datos decodificados no son una lista');
    }
  }

  static deleteItemproduct(String producto_id) async {
    final response = await http
        .delete(Uri.parse('${Utils.baseUrl}/api/products/$producto_id'));

    if (response.statusCode != 200) {
      throw Exception("Error eliminando el producto");
    }
  }

static Future<Producto> addItemproducts(
    String producto_nombre,
    double producto_calorias,
    double producto_proteinas,
    double producto_carbohidratos,
    double producto_grasas,
    int usuario_id) async {
  final response = await http.post(
      Uri.parse('${Utils.baseUrl}/api/products/register'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'producto_nombre': producto_nombre,
        'producto_calorias': producto_calorias,
        'producto_carbohidratos': producto_carbohidratos,
        'producto_proteinas': producto_proteinas,
        'producto_grasas': producto_grasas,
        'usuario_id': usuario_id
      }));

  if (response.statusCode == 200) {
    final dynamic json = jsonDecode(response.body);
    return Producto.fromMap(json);
  } else {
    throw Exception('Failed to add product: ${response.statusCode}, ${response.body}');
  }
}

  static Future<Map<String, dynamic>> updateItemproducto(
      int producto_id,
      String producto_nombre,
      double producto_calorias,
      double producto_proteinas,
      double producto_carbohidratos,
      double producto_grasas,
      int usuario_id) async {
    final response = await http.put(
      Uri.parse('${Utils.baseUrl}/api/products/$producto_id'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'producto_id': producto_id,
        'producto_nombre': producto_nombre,
        'producto_calorias': producto_calorias,
        'producto_carbohidratos': producto_carbohidratos,
        'producto_proteinas': producto_proteinas,
        'producto_grasas': producto_grasas,
        'usuario_id': usuario_id
      }));

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to update product: ${response.body}');
    }
  }

  static dateNow() {
    DateTime now = DateTime.now();
    String fechaSQL = now.toIso8601String().substring(
        0, 10); 
    return fechaSQL;
    print('Fecha en formato SQL: $fechaSQL');
  }

  static addAlimentacion(
      String fecha, int producto, double cantidad, int usuario) async {
    final response = await http.post(Uri.parse('${Utils.baseUrl}/food/ali'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({
          'alimentacion_fecha': fecha,
          'producto_id': producto,
          'alimentacion_cantidad': cantidad,
          'usuario_id': usuario
        }));

    if (response.statusCode == 200) {
      final dynamic json = jsonDecode(response.body);
      print(json);
    } else {
      throw Exception(
          'Error agregando al producto' + response.statusCode.toString());
    }
  }

  static deleteAlimentacion(int idAlimentacion) async {
    final response = await http
        .delete(Uri.parse('${Utils.baseUrl}/food/ali/$idAlimentacion'));

    if (response.statusCode == 200) {
      final dynamic json = jsonDecode(response.body);
      print(json);
    } else {
      throw Exception(
          'Error agregando al producto' + response.statusCode.toString());
    }
  }

  static Future<List<Alimentacion>> getAlimentacion(int usuario) async {
    final response =
        await http.post(Uri.parse('${Utils.baseUrl}/food/dateanduser'),
            headers: {
              'Content-Type': 'application/json',
            },
            body: jsonEncode({'fecha': dateNow(), 'usuario_id': usuario}));

    if (response.statusCode == 200) {
      var decodedData = jsonDecode(response.body);

      alimentacionesMock = [];
      if (decodedData is List) {
        // Recorre cada elemento de la lista
        decodedData.forEach((alimentacion) {
          // Realiza alguna operación con cada producto
          print(alimentacion);
          alimentacionesMock.add(Alimentacion.fromMap(
              alimentacion,
              Controlador.user!,
              Controlador.productosMock.firstWhere((product) =>
                  alimentacion["producto_id"].toString() == product.id)));
        });

        return alimentacionesMock;
      } else {
        throw Exception('Error: los datos decodificados no son una lista');
      }
    } else {
      throw Exception(
          'Error agregando al producto' + response.statusCode.toString());
    }
  }
  static Future userLogin(String email, String Password) async {
    final response = await http.post(Uri.parse('${Utils.baseUrl}/users/login'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({'usuario_email': email, 'usuario_password': Password}));

    print(response.body);
    final List<dynamic> decodedData = jsonDecode(response.body);
    return decodedData;
  }

  static Future userRegister(String username, String email, String password) async {
    final response = await http.post(Uri.parse('${Utils.baseUrl}/users'),
        headers: {"Content-Type": "application/json"},
        body: jsonEncode({
          'usuario_nombre': username,
          'usuario_email': email,
          'usuario_password': password,
        }));
    var decodedData = jsonDecode(response.body);
    return decodedData;
  }
}
