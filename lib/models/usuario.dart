class Usuario {
  final String id;
  final String nombre;
  final String email;
  final String password;



  Usuario({
    required this.id,
    required this.nombre,
    required this.email,
    required this.password,
  });

  @override
  String toString() {
    return 'Usuario{id: $id, nombre: $nombre, email: $email, password: $password}';
  }
}