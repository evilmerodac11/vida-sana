import 'package:flutter/material.dart';
import 'package:login_register/controlador/controlador.dart';
import 'package:login_register/models/producto.dart';
import 'package:login_register/models/usuario.dart';
import 'package:login_register/widgets/form_fields_widgets.dart';

class ProductScreen extends StatefulWidget {
  final Usuario user;
  const ProductScreen(this.user, {Key? key}) : super(key: key);

  @override
  State<ProductScreen> createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  List<Producto> productos = []; // Lista de productos creados por el usuario
  Producto? productoSeleccionado; // Producto seleccionado para editar

  TextEditingController _nombreController = TextEditingController();
  TextEditingController _caloriasController = TextEditingController();
  TextEditingController _proteinasController = TextEditingController();
  TextEditingController _carbohidratosController = TextEditingController();
  TextEditingController _grasasController = TextEditingController();

  @override
  void initState() {
    super.initState();
    productos = _productosInitializer(widget.user);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Align(
            alignment: Alignment.centerRight,
            child: Text(
              "Gestion de Productos",

            ),
          )
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blue,
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: const Text('Agregar producto'),
                  content:SingleChildScrollView(
                    child:Container(
                      child:Column(

                        children: [

                          TextFormField(
                            controller: _nombreController,
                            decoration: InputDecoration(labelText: 'Nombre'),
                          ),
                          TextFormField(
                            controller: _caloriasController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(labelText: 'Calorías'),
                          ),
                          TextFormField(
                            controller: _proteinasController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(labelText: 'Proteínas'),
                          ),
                          TextFormField(
                            controller: _carbohidratosController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(labelText: 'Carbohidratos'),
                          ),
                          TextFormField(
                            controller: _grasasController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(labelText: 'Grasas'),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              // Crear nuevo producto con los datos del formulario
                              Producto nuevoProducto = Producto(
                                id: productoSeleccionado == null ? "000" : productoSeleccionado!.id,
                                nombre: _nombreController.text,
                                calorias: double.tryParse(_caloriasController.text) ?? 0,
                                proteinas: double.tryParse(_proteinasController.text) ?? 0,
                                carbohidratos: double.tryParse(_carbohidratosController.text) ?? 0,
                                grasas: double.tryParse(_grasasController.text) ?? 0,
                                idUsuario: widget.user.id,
                              );
                              setState(()  {

                                if (productoSeleccionado == null) {
                                  productos.add(nuevoProducto);
                                  Controlador.addItemproducts(nuevoProducto.nombre, nuevoProducto.calorias, nuevoProducto.proteinas, nuevoProducto.carbohidratos, nuevoProducto.grasas, int.parse(nuevoProducto.idUsuario))
                                      .then((Producto productoAgregado) {
                                    // Manejar el resultado del Future aquí

                                    Controlador.getAllProductos();

                                  });
                                  Navigator.pop(context, true);

                                  showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      title: Text('Mensaje'),
                                      content: Text('Producto anadido correctamente'),
                                      actions: [
                                        ElevatedButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text('Aceptar'))
                                      ],
                                    ),
                                  );
                                } else {
                                  // Encuentra el índice del producto seleccionado en la lista
                                  int index = productos.indexOf(productoSeleccionado!);
                                  if (index != -1) {
                                    // Reemplaza el producto seleccionado con el nuevo producto
                                    Controlador.updateItemproducto(int.parse(nuevoProducto.id), nuevoProducto.nombre, nuevoProducto.calorias, nuevoProducto.proteinas, nuevoProducto.carbohidratos, nuevoProducto.grasas, int.parse(nuevoProducto.idUsuario));
                                    productos[index] = nuevoProducto;
                                    // También actualiza el producto en la lista de productos mock
                                    int mockindex = Controlador.productosMock.indexOf(productoSeleccionado!);

                                    Controlador.productosMock[mockindex] = nuevoProducto;
                                  }
                                }
                              });
                              _limpiarCampos();
                            },

                            child: Text(productoSeleccionado == null ? 'Crear Producto' : 'Editar Producto'),
                          ),
                        ],
                      ),
                    ),
                  ),


                  actions: [

                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('Cancelar'),
                    ),

                  ],
                );
              });
        },
        tooltip: 'Create un producto',
        child: const Icon(Icons.add),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: 10.0, //Card
          ),
          /*Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                TextFormField(
                  controller: _nombreController,
                  decoration: InputDecoration(labelText: 'Nombre'),
                ),
                TextFormField(
                  controller: _caloriasController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(labelText: 'Calorías'),
                ),
                TextFormField(
                  controller: _proteinasController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(labelText: 'Proteínas'),
                ),
                TextFormField(
                  controller: _carbohidratosController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(labelText: 'Carbohidratos'),
                ),
                TextFormField(
                  controller: _grasasController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(labelText: 'Grasas'),
                ),
                ElevatedButton(
                  onPressed: () {
                    // Crear nuevo producto con los datos del formulario
                    Producto nuevoProducto = Producto(
                      id: productoSeleccionado == null ? "000" : productoSeleccionado!.id,
                      nombre: _nombreController.text,
                      calorias: double.tryParse(_caloriasController.text) ?? 0,
                      proteinas: double.tryParse(_proteinasController.text) ?? 0,
                      carbohidratos: double.tryParse(_carbohidratosController.text) ?? 0,
                      grasas: double.tryParse(_grasasController.text) ?? 0,
                      idUsuario: widget.user.id,
                    );
                    setState(()  {
                      if (productoSeleccionado == null) {
                        productos.add(nuevoProducto);
                        MockData.addItemproducts(nuevoProducto.nombre, nuevoProducto.calorias, nuevoProducto.proteinas, nuevoProducto.carbohidratos, nuevoProducto.grasas, int.parse(nuevoProducto.idUsuario))
                            .then((Producto productoAgregado) {
                          // Manejar el resultado del Future aquí

                            MockData.getAllProductos();
                          });

                      } else {
                        // Encuentra el índice del producto seleccionado en la lista
                        int index = productos.indexOf(productoSeleccionado!);
                        if (index != -1) {
                          // Reemplaza el producto seleccionado con el nuevo producto
                          MockData.updateItemproducto(int.parse(nuevoProducto.id), nuevoProducto.nombre, nuevoProducto.calorias, nuevoProducto.proteinas, nuevoProducto.carbohidratos, nuevoProducto.grasas, int.parse(nuevoProducto.idUsuario));
                          productos[index] = nuevoProducto;
                          // También actualiza el producto en la lista de productos mock
                          int mockindex = MockData.productosMock.indexOf(productoSeleccionado!);

                          MockData.productosMock[mockindex] = nuevoProducto;
                        }
                      }
                    });
                    _limpiarCampos();
                  },

                  child: Text(productoSeleccionado == null ? 'Crear Producto' : 'Editar Producto'),
                ),
              ],
            ),
          ),*/
          SizedBox(
            height: 30.0, //Card
          ),
          Container(
            padding: EdgeInsets.all(8.0),
            height: 50,
            width: 100,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.lightBlue,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Tus productos registrados son:',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: productos.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(productos[index].nombre),
                  subtitle: Text('Calorías: ${productos[index].calorias.toString()}'),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      IconButton(
                        color:Colors.green,
                        icon: Icon(Icons.edit),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                            title: const Text('Agregar producto'),
                            content:SingleChildScrollView(
                              child:Container(
                                child:Column(

                                  children: [

                                    TextFormField(
                                      controller: _nombreController,
                                      decoration: InputDecoration(labelText: 'Nombre'),
                                    ),
                                    TextFormField(
                                      controller: _caloriasController,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(labelText: 'Calorías'),
                                    ),
                                    TextFormField(
                                      controller: _proteinasController,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(labelText: 'Proteínas'),
                                    ),
                                    TextFormField(
                                      controller: _carbohidratosController,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(labelText: 'Carbohidratos'),
                                    ),
                                    TextFormField(
                                      controller: _grasasController,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(labelText: 'Grasas'),
                                    ),
                                    ElevatedButton(
                                      onPressed: () {
                                        // Crear nuevo producto con los datos del formulario
                                        Producto nuevoProducto = Producto(
                                          id: productoSeleccionado == null ? "000" : productoSeleccionado!.id,
                                          nombre: _nombreController.text,
                                          calorias: double.tryParse(_caloriasController.text) ?? 0,
                                          proteinas: double.tryParse(_proteinasController.text) ?? 0,
                                          carbohidratos: double.tryParse(_carbohidratosController.text) ?? 0,
                                          grasas: double.tryParse(_grasasController.text) ?? 0,
                                          idUsuario: widget.user.id,
                                        );
                                        setState(()  {
                                          print('IDs de productos en la lista productos:');
                                          productos.forEach((producto) => print(producto.id));
                                          print('IDs de productos en la lista MockData.productosMock:');
                                          Controlador.productosMock.forEach((producto) => print(producto.id));
                                            // Encuentra el índice del producto seleccionado en la lista
                                            int index = productos.indexOf(productoSeleccionado!);
                                            if (index != -1) {
                                              // Reemplaza el producto seleccionado con el nuevo producto
                                              Controlador.updateItemproducto(int.parse(nuevoProducto.id), nuevoProducto.nombre, nuevoProducto.calorias, nuevoProducto.proteinas, nuevoProducto.carbohidratos, nuevoProducto.grasas, int.parse(nuevoProducto.idUsuario));
                                              productos[index] = nuevoProducto;
                                              // También actualiza el producto en la lista de productos mock
                                              int mockindex = Controlador.productosMock.indexOf(productoSeleccionado!);

                                              Controlador.productosMock[mockindex] = nuevoProducto;

                                            }
                                            Navigator.pop(context, true);
                                          showDialog(
                                              context: context,
                                              builder: (context) => AlertDialog(
                                                content: Text('Producto actualizado correctamente'),
                                                actions: [
                                                  ElevatedButton(
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                      child: Text('Aceptar'))
                                                ],
                                              ),
                                            );
                                        });
                                        _limpiarCampos();
                                      },

                                      child: Text(productoSeleccionado == null ? 'Crear Producto' : 'Editar Producto'),
                                    ),
                                  ],
                                ),
                              ),
                            ),


                            actions: [

                              TextButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: const Text('Cancelar'),
                              ),

                            ],
                          ));
                          // Cuando se presiona editar, cargar los datos del producto seleccionado en el formulario
                          setState(() {
                            productoSeleccionado = productos[index];
                            _cargarDatosProductoEnFormulario();
                          });
                        },
                      ),
                      IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                          // Eliminar el producto de la lista
                          setState(() {
                            print('ID del producto a eliminar: ${productos[index].id}');
                            print('IDs de productos en la lista productos:');
                            productos.forEach((producto) => print(producto.id));
                            print('IDs de productos en la lista MockData.productosMock:');
                            Controlador.productosMock.forEach((producto) => print(producto.id));
                            int removeIndex = -1;
                            int mockIndex = -1;

// Buscar el producto por su ID
                            for (int i = 0; i < productos.length; i++) {
                              if (productos[i].id == productos[index].id) {
                                removeIndex = i;
                                print('el removeIndex');
                                print(removeIndex);
                                break;
                              }
                            }


// Verificar si se encontró el producto en ambas listas
                            if (removeIndex >= 0 ) {
                              // Eliminar el producto de las listas
                              productos.removeAt(removeIndex);
                            } else {
                              print('El producto no existe en la lista.');
                            }
                          });
                        },
                        color: Colors.red, // Color rojo para el botón de eliminar
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),

    );
  }

  // Método para cargar los datos del producto seleccionado en el formulario
  void _cargarDatosProductoEnFormulario() {
    if (productoSeleccionado != null) {
      _nombreController.text = productoSeleccionado!.nombre;
      _caloriasController.text = productoSeleccionado!.calorias.toString();
      _proteinasController.text = productoSeleccionado!.proteinas.toString();
      _carbohidratosController.text = productoSeleccionado!.carbohidratos.toString();
      _grasasController.text = productoSeleccionado!.grasas.toString();
    }
  }

  // Método para limpiar los campos del formulario
  void _limpiarCampos() {
    _nombreController.clear();
    _caloriasController.clear();
    _proteinasController.clear();
    _carbohidratosController.clear();
    _grasasController.clear();
    productoSeleccionado = null;
  }

  List<Producto> _productosInitializer(Usuario user) {
    return Controlador.productosMock.where((Producto producto) {

      return producto.idUsuario.compareTo(user.id) == 0;
    }).toList();
  }
}
