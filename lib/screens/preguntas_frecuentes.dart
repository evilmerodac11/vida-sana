import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:login_register/screens/consejos_nutricionales.dart';
import 'package:login_register/screens/macronutrientes.dart';
//import 'package:login_register/screens/page5.dart';
import 'package:login_register/widgets/drawer_menu.dart';

import 'home_page.dart';

class PreguntasFrecuentes extends StatelessWidget{
  final int elusuarioid;
  const PreguntasFrecuentes(this.elusuarioid,{Key? key}) : super(key:key);
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent, // Color de fondo de la barra de la aplicación
        title: Text('App de calorias'), // Título de la barra de la aplicación
      ),
      // bottomNavigationBar: NavigationBar(
      //   backgroundColor: Colors.lightBlueAccent,// Barra de navegación en la parte inferior
      //   indicatorColor: Colors.amber,  // Índice de la pestaña seleccionada
      //   destinations: const <Widget>[ // Lista de destinos para las pestañas
      //     NavigationDestination(
      //       selectedIcon: Icon(Icons.home), // Icono seleccionado para la pestaña "Home"
      //       icon: Icon(Icons.home_outlined), // Icono para la pestaña "Home"
      //       label: 'Productos', // Etiqueta de la pestaña "Home"
      //     ),
      //     NavigationDestination(
      //       icon: Badge(child: Icon(Icons.account_balance_wallet_sharp)), // Icono para la pestaña "Notifications" con un badge
      //       label: 'Conteo', // Etiqueta de la pestaña "Notifications"
      //     ),
      //     NavigationDestination(
      //       icon: Badge( // Icono para la pestaña "Messages" con un badge
      //         label: Text('1'),
      //         child: Icon(Icons.messenger_sharp),
      //       ),
      //       label: 'Perfil', // Etiqueta de la pestaña "Messages"
      //     ),
      //   ],
      // ),
      drawer: DrawerMenu(this.elusuarioid),
      body: Container(
        margin: EdgeInsets.only(left: 20, top: 10, right: 20),
        child:Column(
          children: [
            Text('Bienvenido usuario: '+elusuarioid.toString()),
            Text('Preguntas frecuentes', style: TextStyle(color: Colors.lightBlue, fontSize: 15),),
            SizedBox(height: 10,),
            Expanded(
              child: Container(
                child:ListView.builder(
                  reverse: true,
                  itemCount: 2,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == 0) {
                      return Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          margin: const EdgeInsets.all(8.0),
                          padding: const EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                            color: Colors.blueAccent,
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Text(
                            'Las calorías están en los alimentos y son los mismos sin importar la hora del día. Lo que sí cambia es el tiempo que tenemos para quemar esas calorías. Si come en la mañana, tiene todo el día para moverse y gastar esas calorías consumidas.',
                          ),
                        ),
                      );
                    }
                    return Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: const EdgeInsets.all(8.0),
                        padding: const EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          color: Colors.lightGreenAccent,
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: Text(
                          '¿Hay relación entre calorías y horario?',
                        ),
                      ),
                    );

                  },
                ),
              ),
            ),
            Expanded(
              child: Container(
                child:ListView.builder(
                  reverse: true,
                  itemCount: 2,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == 0) {
                      return Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          margin: const EdgeInsets.all(8.0),
                          padding: const EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                            color: Colors.blueAccent,
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Text(
                            'El requerimiento calórico es personal, depende de la edad, género, nivel de actividad física y si presenta o no alguna condición patológica que aumente las necesidades.',
                          ),
                        ),
                      );
                    }
                    return Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: const EdgeInsets.all(8.0),
                        padding: const EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          color: Colors.lightGreenAccent,
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: Text(
                          '¿Qué cantidad debo consumir al día?',
                        ),
                      ),
                    );

                  },
                ),
              ),
            ),
            Expanded(
              child: Container(
                child:ListView.builder(
                  reverse: true,
                  itemCount: 2,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == 0) {
                      return Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          height: 100.0,
                          margin: EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30.0),
                            image: DecorationImage(
                              image: NetworkImage("https://www.directoriomedicoquito.com/picts/paulina%20gerka.jpg"),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      );
                    }
                    return Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: const EdgeInsets.all(8.0),
                        padding: const EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          color: Colors.lightGreenAccent,
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: Text(
                          '¿Me puedes recomentar a un experto?',
                        ),
                      ),
                    );

                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}