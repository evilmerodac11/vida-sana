import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:login_register/controlador/controlador.dart';
import 'package:login_register/models/alimentacion.dart';
import 'package:login_register/models/producto.dart';
import 'package:login_register/models/usuario.dart';

class ProductListScreen extends StatefulWidget {
  final Usuario user;
  const ProductListScreen(this.user, {super.key});

  @override
  State<ProductListScreen> createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  List<Producto> productos = Controlador.productosMock;
  TextEditingController _searchControler = TextEditingController(text: "");
  TextEditingController _cantidadControler = TextEditingController(text: "0");
  @override
  void initState() {
    super.initState();
    _searchControler.addListener(_onSearchTextChanged);
  }

  @override
  void dispose() {
    _searchControler.dispose();
    super.dispose();
  }

  void _onSearchTextChanged() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: productos.length + 1,
      itemBuilder: (_, index) {
        if (index == 0) {
          return Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: TextField(
                  onChanged: (text) {
                    _onSearchTextChanged();
                    if (text == "") {
                      setState(() {
                        productos = Controlador.productosMock;
                      });
                    }
                    setState(() {
                      productos =
                          Controlador.productosMock.where((Producto producto) {
                        return producto.nombre
                            .toLowerCase()
                            .contains(text.toLowerCase());
                      }).toList();
                    });
                  },
                  controller: _searchControler,
                  decoration: const InputDecoration(hintText: "Buscar..."),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: TextField(
                  controller: _cantidadControler,
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp('[0-9 .,]'))
                  ],
                  decoration: const InputDecoration(hintText: "Cantidad"),
                  onTap: () {
                    _cantidadControler.clear();
                  },
                  onEditingComplete: () {
                    if (_cantidadControler.text.isEmpty) {
                      _cantidadControler.text = "0";
                    }
                  },
                ),
              )
            ],
          );
        }
        return ListTile(
          title: Text(productos[index - 1].nombre),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Calorías: ${productos[index - 1].calorias}'),
              Text('Proteínas: ${productos[index - 1].proteinas}'),
              Text('Carbohidratos: ${productos[index - 1].carbohidratos}'),
              Text('Grasas: ${productos[index - 1].grasas}'),
            ],
          ),
          trailing: ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(Colors.green.shade500)),
            onPressed: () {
              if (double.parse(_cantidadControler.text) != 0) {
                Controlador.addAlimentacion(
                    Controlador.dateNow(),
                    int.parse(productos[index - 1].id),
                    double.parse(_cantidadControler.text),
                    int.parse(widget.user.id));
              }
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    backgroundColor: Colors.green,
                    title: Row(
                      children: [
                        Icon(Icons.local_dining), // Icono de nutrición
                        SizedBox(width: 10),
                        Text('Éxito'),
                      ],
                    ),
                    content: Text(
                      'El producto seleccionado se ha agregado correctamente para el conteo de calorías.',
                      style: TextStyle(color: Colors.white),
                    ),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text('Aceptar', style: TextStyle(color: Colors.white)),
                      ),
                    ],
                  );
                },
              );
              setState(() {
                Controlador.alimentacionesMock.add(Alimentacion(
                    fecha: Controlador.dateNow(),
                    producto: productos[index - 1],
                    cantidad: double.parse(_cantidadControler.text),
                    usuario: widget.user));
              });
            },
            child: Text(
              'Agregar',
              style: TextStyle(color: Colors.white),
            ),
          ),
        );
      },
    );
  }

  _deleteItem() {}
}
