import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:login_register/constant/utils.dart';
import 'package:login_register/controlador/controlador.dart';
import 'package:login_register/models/usuario.dart';
import 'package:login_register/screens/home_page.dart';
import 'package:login_register/widgets/form_fields_widgets.dart';
import 'package:http/http.dart' as http;
import '../item.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  final String serverUrl = Utils.baseUrl;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final nameController = TextEditingController();
  final theemailController = TextEditingController();
  final thepasswordController = TextEditingController();

  bool _isLoading = false;

  void addItem(String usuario_nombre, String usuario_email, String usuario_password) async {
    Controlador.userRegister(usuario_nombre, usuario_email, usuario_password);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.white, Colors.black12],
                  begin: const FractionalOffset(0.0, 1.0),
                  end: const FractionalOffset(0.0, 1.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.repeated,
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(height: 100),
                  SizedBox(height: 10),
                  Text(
                    'Inicio de sesión',
                    style: TextStyle(
                      color: Colors.purple,
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                    ),
                  ),
                  SizedBox(height: 40),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        FormFields(
                          controller: _emailController,
                          data: Icons.email,
                          txtHint: 'Email',
                          obsecure: false,
                        ),
                        FormFields(
                          controller: _passwordController,
                          data: Icons.lock,
                          txtHint: 'Password',
                          obsecure: true,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Te olvidaste el password?',
                        style: TextStyle(color: Colors.white, fontSize: 12),
                      ),
                      SizedBox(width: 15),
                      ElevatedButton(
                        onPressed: () {
                          if (_emailController.text.isNotEmpty && _passwordController.text.isNotEmpty) {
                            setState(() {
                              _isLoading = true;
                            });
                            doLogin(_emailController.text, _passwordController.text);
                          } else {
                            Fluttertoast.showToast(
                              msg: 'Todos los campos son requeridos',
                              textColor: Colors.red,
                            );
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.purple,
                          padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                          textStyle: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        child: Text("Iniciar sesión", style: TextStyle(color: Colors.white)),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ),
          if (_isLoading)
            Center(
              child: CircularProgressIndicator(),
            ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: const Text('Agregar usuario'),
                content: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: [
                        TextFormField(
                          controller: nameController,
                          decoration: const InputDecoration(
                            labelText: 'Nombre',
                          ),
                        ),
                        TextFormField(
                          controller: theemailController,
                          decoration: const InputDecoration(
                            labelText: 'Email',
                          ),
                        ),
                        TextFormField(
                          controller: thepasswordController,
                          decoration: const InputDecoration(
                            labelText: 'Password',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('Cancelar'),
                  ),
                  TextButton(
                    onPressed: () {
                      addItem(nameController.text, theemailController.text, thepasswordController.text);
                      setState(() {
                        nameController.clear();
                        theemailController.clear();
                        thepasswordController.clear();
                      });
                      Navigator.pop(context);
                    },
                    child: const Text('Guardar'),
                  ),
                ],
              );
            },
          );
        },
        tooltip: 'Crear una cuenta',
        child: const Icon(Icons.add),
      ),
    );
  }

  doLogin(String email, String password) async {
    try {
      final res = await Controlador.userLogin(email.trim(), password.trim());

      if (res != null && res.toString().contains('id')) {
        var este = res.toString().substring(14, 16).replaceAll(RegExp('}'), '');
        int userId = int.parse(este.trim());
        Controlador.user = Usuario(id: userId.toString(), nombre: email, email: email, password: password);
        await Controlador.getAllProductos();

        setState(() {
          _isLoading = false;
        });

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (_) => PageStatefull(userId)),
        );
      } else {
        setState(() {
          _isLoading = false;
        });
        Fluttertoast.showToast(msg: 'Error en el inicio de sesión', textColor: Colors.red);
      }
    } catch (e) {
      setState(() {
        _isLoading = false;
      });
      Fluttertoast.showToast(msg: 'Error: $e', textColor: Colors.red);
    }
  }
}
