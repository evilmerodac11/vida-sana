

class ItemProducto {
  final int producto_id;
  final String producto_nombre;
  final num producto_calorias;
  final num producto_proteinas;
  final num producto_grasas;
  final int usuario_id;
  ItemProducto({required this.producto_id, required this.producto_nombre, required this.producto_calorias, required this.producto_proteinas, required this.producto_grasas, required this.usuario_id});

  factory ItemProducto.fromJson(Map<String, dynamic> json) {
    return ItemProducto(producto_id: json['producto_id'], producto_nombre: json['producto_nombre'], producto_calorias: json['producto_calorias'], producto_proteinas: json['producto_proteinas'], producto_grasas: json['producto_grasas'], usuario_id: json['usuario_id']);
  }
}